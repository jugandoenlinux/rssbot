#!/usr/bin/env python3
# ! python3
# -*- coding: utf-8 -*-
#
# P_Vader, Serjor, Son Link, Odin para JEL 2021 (c)
#
# Basado en script desde https://lordpedal.github.io
# Gist original: https://gist.github.com/ljmocic/ec013451ff8bbb51f308d79975d7fdb4

# Librerias Python
import datetime as dt
from dateutil.tz import UTC
from dateutil import parser
from time import sleep
from bs4 import BeautifulSoup
from urllib.parse import quote
from os import path
import requests
import feedparser
import logging
import json
import tweepy
import os
import sys

# LOG
logging.basicConfig(level=logging.DEBUG, filename='rss_bot.log')
hora_inicio = dt.datetime.now(dt.timezone.utc)
logging.info(f'Iniciado: {hora_inicio}')


def check_publish(published_date, delta):
    utc_current = dt.datetime.now(dt.timezone.utc)
    parsed_published_date = (parser.parse(published_date)).astimezone(UTC)
    published_minutes_ago = utc_current - parsed_published_date <= dt.timedelta(minutes=delta)
    logging.info(f'Fecha UTC ultima public.={parsed_published_date} Hora actual UTC={utc_current} Minutos hace={published_minutes_ago}')
    return published_minutes_ago


def get_script_path():
    return os.path.dirname(os.path.realpath(sys.argv[0]))


def get_configuration(file_path):
    with open(file_path, "r") as json_file:
        configuration = json.load(json_file)
    return configuration


# Twitter
def send_message_twitter(message, image, configuration):
    logging.info('send message twitter')
    auth = tweepy.OAuthHandler(configuration["consumer_key"], configuration["consumer_secret"])
    auth.set_access_token(configuration["access_token"], configuration["access_token_secret"])
    api = tweepy.API(auth)
    api.update_status_with_media(status=message, filename=image)
    logging.info('publicado mensaje en twitter')


def send_message_telegram(message, url_photo, configuration):
    telegram_token_bot = configuration['telegram_token_bot']
    telegram_contacto_id = configuration['telegram_contacto_id']
    telegram_response = requests.get(
        f'https://api.telegram.org/bot{telegram_token_bot}/sendPhoto?chat_id={telegram_contacto_id}&photo={url_photo}&caption={message}&disable_web_page_preview=true&parse_mode=Markdown')
    logging.info(
        f'Enviado a telegram: https://api.telegram.org/bot{telegram_token_bot}/sendPhoto?chat_id={telegram_contacto_id}&photo={url_photo}&caption={message}&disable_web_page_preview=true&parse_mode=Markdown')
    telegram_message = telegram_response.json()
    telegram_message_id = telegram_message['result']['message_id']
    requests.get(
        f'https://api.telegram.org/bot{telegram_token_bot}/pinChatMessage?chat_id={telegram_contacto_id}&message_id={telegram_message_id}')
    logging.info(
        f'https://api.telegram.org/bot{telegram_token_bot}/pinChatMessage?chat_id={telegram_contacto_id}&message_id={telegram_message_id}')


def send_status_mastodon(message, image, configuration):
    filename = path.basename(image)
    media_ids = []
    data = {
        'description': 'Imagen de portada ' + image
    }

    files = {
        'file': (filename, open(image, 'rb'), 'application/octet-stream')
    }
    url = "%s/api/v1/media" % (configuration['mastodon_host'])
    r = requests.post(url,
                      files=files,
                      headers={'Authorization': 'Bearer %s' % (configuration['mastodon_token'])}
                      )
    json_data = r.json()
    logging.info(json_data)

    media_id = json_data['id']
    media_ids.append(media_id)

    # after collecting the media ids, include them in the toot payload
    data = {
        "status": message,
        "media_ids[]": media_ids
    }

    url = "%s/api/v1/statuses" % (configuration['mastodon_host'])
    r = requests.post(url,
                      data=data,
                      headers={'Authorization': 'Bearer %s' % (configuration['mastodon_token'])})
    json_data = r.json()
    logging.info(json_data)


def main(configuration):
    rss_feed = feedparser.parse(configuration['rss_url'])
    for entry in rss_feed.entries:
        if check_publish(entry.published, configuration['plazo_minutos']):
            soup = BeautifulSoup(entry.description, features="html.parser")
            images = soup.findAll('img')
            image = images[0]['src']
            for script in soup(["script", "style"]):
                script.extract()
            text = soup.get_text()
            text_no_arroba = text.translate({ord('@'): None})
            text_description_clean = quote(text_no_arroba)
            text_title_clean = quote(entry.title)
            response_download_photo = requests.get(image)
            if response_download_photo.status_code:
                fp = open('image_temp.jpg', 'wb')
                fp.write(response_download_photo.content)
                fp.close()
            if configuration['enviar_telegram']:
                send_message_telegram(
                    '*Noticias JugandoEnLinux.com:*\n' + '\n\n' + '*' + text_title_clean + '*' + '\n\n' + text_description_clean + '\n' +
                    entry.links[0].href + '\n\n\n' + '%23linux %23linuxgaming', image, configuration['telegram'])
            else:
                logging.info("desactivado enviar a telegram")
            if configuration['enviar_mastodon']:
                send_status_mastodon(
                    entry.title + '\n\n' + text_no_arroba + '\n' + entry.links[0].href + '\n\n' + '#linux #linuxgaming',
                    'image_temp.jpg', configuration['mastodon'])
            else:
                logging.info("desactivado enviar a mastodon")
            if configuration['enviar_twitter']:
                send_message_twitter(
                    entry.title + '\n\n' + text_no_arroba + '\n' + entry.links[0].href + '\n\n' + '#linux #linuxgaming',
                    'image_temp.jpg', configuration['twitter'])
            else:
                logging.info("desactivado enviar a twitter")


if __name__ == "__main__":
    try:
        config = get_configuration(os.path.join(get_script_path(), "configuration.json"))
        while True:
            main(config)
            sleep(config['plazo_minutos'] * 60)
    except IOError as ioe:
        logging.error(ioe)
