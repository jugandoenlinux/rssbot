# RSS Bot

Un maravilloso bot que te publica en redes sociales desde el feed RSS.

## POR HACER:

- [X] Sacar token a archivo externo.

- [ ] Añadir el path para el log y la imagen. Se guarda en la raiz del usuario, no en la carpeta de la aplicación.

- [ ] Añadir max size al archivo log.

- [ ] Sumar un minuto a los ultimos minutos revisados. Si despierta cada 5 minutos que revise los ultimos 6 minutos. Puede publicar dos veces. Añadir variable para controlar que en la anterior comprobacion se publico el articulo?

- [ ] Usar SystemD en lugar de cron para manejar el bot.

- [ ] En la web a veces incluimos el usuario de redes sociales tipo @user para linkar. Se limpia el caracter @ para que en telegram no vincule a un usuario no relacionado.  Mastodon y Twitter no van a coincidir el mismo @user tampoco, pero ¿Deberiamos dejar que se publique en twitter y mastodon con @?

